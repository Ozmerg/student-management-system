# Student Management System (SMS)

This project shall provide a playground for discovering and experimenting with frameworks.

## Backend

To build a distributed application in a timely manner, Spring Boot / Spring Cloud was selected as the underlying backend framework. Due to the widespread use of Maven as the build management and automation tool in Java projects I also considered to use it in this application. But one should remember, Gradle outperforms Maven with it's incremental building capabilities.

For persistence purposes a relational db (postgresql) is used in combination with liquibase as database migration tool. The idea is to provide every microservice (which is managing entities) with its own db running in a docker container. A rudimentary docker-compose file spins up the required postgresql databases for the person and pwd services

For building a coping mechanism in case of an unreachable or unavailable service during an transaction which spans over multiple services, I figured to try out Apache Kafka as a message broker. I somewhat forced an example into existence by splitting up the user information from the password information into separate microservices.

In case of unit testing I considered to build regression tests first. This unfortunately has one major drawback. I was unable to integrate mutation tests into my existing tests (e.g.: PiTest), which would raise the quality of the unit tests considerably.

---

<div align="center">
  <img src="prjResources/architecture/sms-project.png">
</div>

---

The information flow between the existing services is locked down to the paths shown in the image above. A client outside the backend environment is only allowed to call deeper tier services through the gateway service. Additionally the client needs to be supplied with the current keystore which is corresponding with the certificate included in the truststore of the gateway service.

Due to testing reasons it is called for now "test.p12" to be found in the directory "ca/testing/". Even with the keystore provided, the client will only by able to communicate with the gateway service, because of the mutual TLS **without** certificate authority configuration.

To retrieve data from e.g. the person database the incoming request will be validated for a jwt token. This token is generated during an successful login in the auth service and returned to the client. Outdated tokens are granted a chance to be "refreshed". Currently a token can be refreshed if the provided token is no longer than two hours expired. Beyond two hours a new token needs to be generated via the login mechanism.

## Frontend

Coming soon.
