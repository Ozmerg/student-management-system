package integration.token;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.time.LocalDate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.web.client.HttpClientErrorException;

import app.GatewayApplication;
import general.dto.operationRequests.RegisterRequest;
import general.dto.personEntities.Person;
import general.dto.pwdEntities.Pwd;
import general.errorHandling.ErrorDetails;
import general.security.JwtConfig;
import general.security.Roles;

@SpringJUnitConfig
@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
@SpringBootTest(classes = GatewayApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, properties = {
        "security.jwt.expiration=0" })
public class TokenIntegrationTest {

    private static TestRestTemplate testRestTemplate;

    @LocalServerPort
    private int port;

    private String getRootUrl() {
        return "http://localhost:" + port;
    }

    private static RegisterRequest registerRequest;

    private static String[] authBody;

    private static String token;

    private static final String REFRESH_TOKEN_PATH = "/auth/refreshtoken";

    private static final String LOGIN_PATH = "/auth";

    private static final String REGISTER_PATH = "/auth/register";

    private static final String ALL_PERSON_PATH = "/person";

    @Autowired
    private JwtConfig jwtConfig;

    @BeforeAll
    public static void setUp() {
        testRestTemplate = new TestRestTemplate();
        testRestTemplate.getRestTemplate()
                .setErrorHandler(new org.springframework.web.client.DefaultResponseErrorHandler());
        registerRequest = RegisterRequest.builder()
                .person(Person.builder().firstName("Steve").lastName("Zoll").birthDate(LocalDate.of(1994, 06, 12))
                        .emailAddress("stevezoll@google.com").role(Roles.ADMIN.name()).build())
                .pwd(Pwd.builder().prePwd("!QAY2wsx§EDC32").build()).build();
        authBody = new String[] { "stevezoll@google.com", "!QAY2wsx§EDC32" };
    }

    @Order(1)
    @Test
    public void registerTest() throws JsonProcessingException {
        HttpEntity<RegisterRequest> registerEntity = new HttpEntity<>(registerRequest, getHeaders());
        try {
            ResponseEntity<String> response = testRestTemplate.postForEntity(getRootUrl() + REGISTER_PATH,
                    registerEntity, String.class);
            assertTrue(
                    response.getBody()
                            .contains("{\"registration complete for user: "
                                    + registerRequest.getPerson().getEmailAddress() + "\":true}"),
                    "The registration confirm message was either changed or something went wrong during the registration process");
            assertTrue(response.getStatusCode().equals(HttpStatus.OK),
                    "This is not the expected status code. It should be the code: 200 ");
        } catch (HttpClientErrorException e) {
            fail("The registration process should run smoothly! " + e.getMessage());
        }
    }

    @Order(2)
    @Test
    public void failOnRefreshTokenRetrieveWithoutToken() {
        HttpEntity<String[]> loginEntity = new HttpEntity<>(authBody, getHeaders());
        try {
            testRestTemplate.postForEntity(getRootUrl() + REFRESH_TOKEN_PATH, loginEntity, String.class);
            fail("The post operation on " + REFRESH_TOKEN_PATH
                    + " must result in an error because no token was provided!");
        } catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.UNAUTHORIZED, e.getStatusCode(),
                    "The expected status of the response should be \"Unauthorized\"");
        }
    }

    @Order(3)
    @Test
    public void receiveTokenTest() {
        HttpEntity<String[]> loginEntity = new HttpEntity<>(authBody, getHeaders());
        sendRequestForToken(loginEntity, LOGIN_PATH);
    }

    @Order(4)
    @Test
    public void failOnDataRetrieveTest() throws IOException {
        isTokenCreated(token);
        HttpEntity<String> requestEntity = new HttpEntity<>(null, getHeaders());
        try {
            testRestTemplate.exchange(getRootUrl() + ALL_PERSON_PATH, HttpMethod.GET, requestEntity, String.class);
            fail("The authentication process should fail at this point! There should be an exception thrown which defines an "
                    + " expired jwt which was returned while accessing raw data with /person"
                    + " Are you really using the application-test.yaml of authService? ");
        } catch (HttpClientErrorException e) {
            ErrorDetails details = new ObjectMapper().readValue(e.getResponseBodyAsString(), ErrorDetails.class);
            assertTrue(details.getException().contains("JWT expired at"),
                    "Wrong exception provided. Should be of typ: ExpiredJwtException. Details: " + details.toString());
        }
    }

    @Order(5)
    @Test
    public void refreshTokenTest() {
        isTokenCreated(token);
        HttpEntity<String> requestEntity = new HttpEntity<>(null, getHeaders());
        sendRequestForToken(requestEntity, REFRESH_TOKEN_PATH);
    }

    @Order(6)
    @Test
    public void successOnDataRetrieveTest() {
        isTokenCreated(token);
        HttpEntity<String> requestEntity = new HttpEntity<>(null, getHeaders());
        try {
            ResponseEntity<String> response = testRestTemplate.exchange(getRootUrl() + ALL_PERSON_PATH, HttpMethod.GET,
                    requestEntity, String.class);
            assertTrue(response.getBody().contains(registerRequest.getPerson().getEmailAddress()),
                    "Something went wrong during the earlier stages of this integration test. Please make sure the provided dummy was correctly stored in the database.");
        } catch (HttpClientErrorException e) {
            fail("This operation should run smoothly, because we now should have a valid token! Details: "
                    + e.getMessage());
        }
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        if (token != null) {
            headers.set(jwtConfig.getHeader(), token);
        }
        return headers;
    }

    private void isTokenCreated(String token) {
        assertNotNull(token,
                "The previous test hasn't run correctly. You are not allowed/unable to run this single test without the previous ones. This failure is a symptome not the cause!.");
        assertTrue(token.startsWith(jwtConfig.getPrefix()),
                "Something must have went wrong during the previous tests. This failure is mere a symptom not the cause!");
    }

    private void sendRequestForToken(HttpEntity<?> loginEntity, String path) {
        try {
            ResponseEntity<String> response = testRestTemplate.postForEntity(getRootUrl() + path, loginEntity,
                    String.class);
            token = response.getHeaders().get(jwtConfig.getHeader()).get(0);
            assertNotNull(token, "Token must not be null");
            assertTrue(!token.isEmpty(), "Token must not be empty");
            assertTrue(token.startsWith(jwtConfig.getPrefix()), "Token must start with " + jwtConfig.getPrefix());
            assertTrue(jwtConfig.validateToken(token), "Token must be valid");
        } catch (NullPointerException | HttpClientErrorException e) {
            fail("The token acquisition should run smoothly! " + e.getMessage());
        }
    }
}
