package app;

import java.util.Locale;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;

import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import general.security.EurekaHostnameVerifier;

@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class,
        KafkaAutoConfiguration.class })
@EnableEurekaClient
@EnableZuulProxy
public class GatewayApplication {

    @Value("${server.ssl.trust-store-password}")
    private String trustStoreKey;

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

    @Bean
    public SSLContext sslContext() throws Exception {
        return new SSLContextBuilder()
                .loadTrustMaterial(ResourceUtils.getFile("classpath:x509/gatewaytruststore.p12"),
                        trustStoreKey.toCharArray())
                .loadKeyMaterial(ResourceUtils.getFile("classpath:x509/gateway.p12"), trustStoreKey.toCharArray(),
                        trustStoreKey.toCharArray())
                .build();
    }

    @Bean
    public HostnameVerifier getEurekaHostnameVerifier() {
        return new EurekaHostnameVerifier();
    }

    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.US);
        return slr;
    }
}
