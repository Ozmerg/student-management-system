package app.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.context.RequestContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import general.security.JwtConfig;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.impl.DefaultClaims;

public class JwtTokenAuthenticationFilter extends OncePerRequestFilter {

    private static final Logger LOG = LoggerFactory.getLogger(JwtTokenAuthenticationFilter.class);

    private final JwtConfig jwtConfig;

    private String headerValue;

    public final static String REFRESH_TOKEN_PATH = "/auth/refreshtoken";

    public JwtTokenAuthenticationFilter(JwtConfig jwtConfig) {
        this.jwtConfig = jwtConfig;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        headerValue = request.getHeader(jwtConfig.getHeader());
        if (!isSmsToken()) {
            chain.doFilter(request, response);
            return;
        }
        String token = removeSmsPrefix();
        Claims claims = parseClaimsFromToken(request, response, token);
        authenticateWithClaims(claims);
        chain.doFilter(request, response);
    }

    private String removeSmsPrefix() {
        return headerValue.replace(jwtConfig.getPrefix(), "");
    }

    private boolean isSmsToken() {
        return headerValue == null ? false : headerValue.startsWith(jwtConfig.getPrefix());
    }

    private Claims parseClaimsFromToken(HttpServletRequest request, HttpServletResponse response, String token)
            throws IOException {
        try {
            return Jwts.parser().setSigningKey(jwtConfig.getSecret().getBytes()).parseClaimsJws(token).getBody();
        } catch (SignatureException | MalformedJwtException | UnsupportedJwtException | IllegalArgumentException e) {
            handleFilterException(request, response, e);
        } catch (ExpiredJwtException e) {
            if (isAllowedToRefreshToken(request, e)) {
                if (LOG.isDebugEnabled())
                    LOG.debug("Token expired. Refresh allowed.");
                RequestContext cxt = RequestContext.getCurrentContext();
                cxt.addZuulRequestHeader("claims", new ObjectMapper().writeValueAsString(e.getClaims()));
                authenticateWithClaims(e.getClaims());
            } else {
                handleFilterException(request, response, e);
            }
        }
        return new DefaultClaims();
    }

    private void handleFilterException(HttpServletRequest request, HttpServletResponse response, Exception e)
            throws IOException {
        LOG.warn(e.getMessage());
        SecurityContextHolder.clearContext();
        request.setAttribute("exception", e.getMessage());
        JwtAuthenticationEntryPoint.prepareErrorResponse(request, response, "Couldn't finish authentication process");
    }

    private boolean isAllowedToRefreshToken(HttpServletRequest request, ExpiredJwtException e) {
        return e.getClaims().getExpiration().getTime() + jwtConfig.getRefreshExpiration() >= new Date().getTime()
                && request.getRequestURI().contains(REFRESH_TOKEN_PATH);
    }

    private void authenticateWithClaims(Claims claims) {
        String username = claims.getSubject();
        if (username != null) {
            List<String> authorities = extractAuthorities(claims.get("authorities"));
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(username, null,
                    authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
            SecurityContextHolder.getContext().setAuthentication(auth);
            if (LOG.isDebugEnabled())
                LOG.debug("Provided token was authenticated. Proceed.");
        }
    }

    private List<String> extractAuthorities(Object object) throws IllegalArgumentException {
        List<?> result = object instanceof ArrayList<?> ? (ArrayList<?>) object : null;
        if (result != null) {
            return Arrays.asList(result.toArray(new String[result.size()]));
        }
        throw new IllegalArgumentException("Could not extract authorities from token claims");
    }
}