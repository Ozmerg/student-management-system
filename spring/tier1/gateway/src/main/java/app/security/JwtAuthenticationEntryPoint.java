package app.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import general.errorHandling.ErrorDetails;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

        @Override
        public void commence(HttpServletRequest request, HttpServletResponse response,
                        AuthenticationException authException) throws IOException, ServletException {
                prepareErrorResponse(request, response, authException.getMessage());
        }

        public static void prepareErrorResponse(HttpServletRequest request, HttpServletResponse response,
                        String message) throws IOException {
                final String exception = request.getAttribute("exception") == null ? "n/a"
                                : (String) request.getAttribute("exception");
                String json = new ObjectMapper().writeValueAsString(ErrorDetails.builder().message(message)
                                .path(request.getRequestURI()).exception(exception).build());
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }

}
