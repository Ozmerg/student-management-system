package controller;

import app.PersonApplication;
import com.fasterxml.jackson.databind.ObjectMapper;
import general.dto.personEntities.Person;
import general.errorHandling.ErrorDetails;
import general.errorHandling.ResourceNotFoundException;
import general.security.Roles;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
@SpringBootTest(classes = PersonApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@ActiveProfiles(value = "dev")
public class PersonControllerIT {

    private static TestRestTemplate testRestTemplate;
    private static List<Person> dummies;
    private static List<Person> badDummies;
    //    private MockMvc mockMvc;
//    @Autowired
//    private ObjectMapper mapper;
    @LocalServerPort
    private int port;

    @BeforeAll
    public static void setUp() {
        testRestTemplate = new TestRestTemplate();
        testRestTemplate.getRestTemplate()
                .setErrorHandler(new org.springframework.web.client.DefaultResponseErrorHandler());
        dummies = new ArrayList<>();
        dummies.add(Person.builder().birthDate(LocalDate.of(1995, 1, 1)).emailAddress("dummy@dummy.com")
                .firstName("Dummy-dummy").lastName("Dummy").role(Roles.STUDENT.name()).build());
        dummies.add(Person.builder().birthDate(LocalDate.of(1992, 7, 5)).emailAddress("hallo@world.com")
                .firstName("hello").lastName("world").role(Roles.STUDENT.name()).build());
        badDummies = List.of(
                // Invalid firstName
                Person.builder().birthDate(LocalDate.of(1995, 1, 1)).emailAddress("dum2my@dummy.com")
                        .firstName("Dummy2").lastName("Dummy").role(Roles.STUDENT.name()).build(),
                // Invalid lastName
                Person.builder().birthDate(LocalDate.of(1995, 1, 1)).emailAddress("dum2my@dummy.com")
                        .firstName("Dummy").lastName("Dummy2").role(Roles.STUDENT.name()).build(),
                // Invalid lastName & firstName
                Person.builder().birthDate(LocalDate.of(1995, 1, 1)).emailAddress("dum2my@dummy.com")
                        .firstName(" Dum2my").lastName("D ummy2").role(Roles.STUDENT.name()).build(),
                // Already existing Email
                Person.builder().birthDate(LocalDate.of(1959, 1, 2)).emailAddress("Dummy@Dummy.com")
                        .firstName("richi").lastName("richards").role(Roles.STUDENT.name()).build(),
                // Param missing (role)
                Person.builder().birthDate(LocalDate.of(1997, 1, 1)).emailAddress("rich@dummy.com").firstName("jonas")
                        .lastName("richards").build(),
                // Email address invalid
                Person.builder().birthDate(LocalDate.of(1997, 1, 1)).emailAddress("richdummy.com").firstName("richi")
                        .lastName("richards").role(Roles.STUDENT.name()).build(),
                // Exceeding max length of email
                Person.builder().birthDate(LocalDate.of(1997, 1, 1)).emailAddress(
                        "aknokndojnn23r3ur20ifn1ßeiosndjnffaokndfnfdoknfnnoi2do2nfjaljdsdksqlkdsqldsdsdwejnlwlfmo2nflkdlksnlkdnejmd2oijroi3nfldnflkdf@duskjdnskdjqdqdbdibscbsjbsjnckjcdclkqnccknslkndw@clkncidwkncimmy.com")
                        .firstName("rolf").lastName("Jensen").role(Roles.STUDENT.name()).build());
    }

    private String getRootUrl() {
        return "http://localhost:" + port;
    }

    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext,
                      RestDocumentationContextProvider restDocumentation) {
//        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
//                .apply(documentationConfiguration(restDocumentation)).alwaysDo(document("{method-name}",
//                        preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint())))
//                .build();
    }

    @Test
    @Order(1)
    public void testcreatePerson() throws IOException {
        for (Person dummy : dummies) {
            Person dbPerson = testRestTemplate.postForObject(getRootUrl() + "/person/", dummy, Person.class);
            Collections.replaceAll(dummies, dummy, dbPerson);
        }

        Map<String, Integer> catchedConstraintViolations = new HashMap<>();
        for (Person user2fail : badDummies) {
            try {
                testRestTemplate.postForObject(getRootUrl() + "/person/", user2fail, Person.class);
            } catch (final HttpClientErrorException e) {
                ErrorDetails details = new ObjectMapper().readValue(e.getResponseBodyAsString(), ErrorDetails.class);
                Integer i = catchedConstraintViolations.putIfAbsent(details.getException(), 1);
                if (i != null) {
                    catchedConstraintViolations.put(details.getException(), i + 1);
                }
            }
        }
        assertEquals(catchedConstraintViolations.get("MethodArgumentNotValidException"), 6,
                "There must be 6 exceptions defining illegal arguments detected while trying to post the bad dummies."
                        + " Please contact the author of entity person to verify your changes!!");
        assertEquals(catchedConstraintViolations.get("DataIntegrityViolationException"), 1,
                "There must be 1 sql exceptions defining illegal arguments detected while trying to post the bad dummies."
                        + " Please contact the author of person to verify your changes!!");
    }

    @Test
    @Order(2)
    public void testGetAllDummyPersons() {

        Person[] dbDummies = testRestTemplate.getForObject(getRootUrl() + "/person/", Person[].class);
        assertTrue((Arrays.asList(dbDummies).containsAll(dummies)),
                "There is a problem with the findAll Persons function. Please check if test Person creation run correctly");
    }

    @Test
    @Order(3)
    public void testGetPersonByEmail() {
        for (Person person : dummies) {
            Person user = getPersonByUsername(person.getEmailAddress());
            assertNotNull(user.getId(),
                    "Basic search of single Person is not working properly. The good dummy of Name: "
                            + person.getFirstName() + " " + person.getLastName() + "should have been found");
            assertEquals(person, user,
                    "There is a problem with the findByUsername Persons function. Please check if test Person creation run correctly");
        }
        for (Person badDummy : badDummies) {
            try {
                Person dbPerson = getPersonByUsername(badDummy.getEmailAddress());
                if (dbPerson != null && !dbPerson.getFirstName().equalsIgnoreCase("richi")) {
                    fail("This message was raised because one of the bad dummies "
                            + " (except the bad dummy 'richi', he has the same email like one of the good dummies)"
                            + " made it into the database. This is not allowed to happen at any given moment in time!"
                            + " There needs to be a thorough investigation on how " + badDummy.getFirstName() + " "
                            + badDummy.getLastName() + " got into the database");
                }
            } catch (HttpClientErrorException e) {
                assertNotNull(e);
            }
        }
    }

    @Test
    @Order(4)
    public void testGetPersonByFirstAndLastname() {
        for (Person person : dummies) {
            Person user = getPersonByUsername(person.getFirstName() + "_" + person.getLastName());
            assertNotNull(user.getId(),
                    "Basic search of single Person is not working properly. The good dummy of name: "
                            + person.getFirstName() + " " + person.getLastName() + "should have been found");
            assertEquals(person, user,
                    "There is a problem with the findByUsername function. Please check if test Person creation run correctly");
        }

        for (Person person : badDummies) {
            try {
                person = getPersonByUsername(person.getFirstName() + "_" + person.getLastName());
                fail("This message was raised because one of the bad dummies made it into the database. This is not allowed to happen at any given moment in time!"
                        + " There needs to be an investigation to find out how " + person.getFirstName() + " "
                        + person.getLastName() + " with id: " + person.getId() + " got into the database");
            } catch (HttpClientErrorException e) {
                assertNotNull(e);
                assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
            }
        }
    }

    @Test
    @Order(5)
    public void testUpdatePerson() {
        Person user = getPersonByUsername(dummies.get(0).getEmailAddress());
        user.setFirstName("Arnold");
        user.setLastName("Schwarzer");
        testRestTemplate.put(getRootUrl() + "/person/" + user.getId(), user);
        Person user2 = getPersonByUsername(user.getEmailAddress());
        assertTrue(user2.getFirstName().equals("Arnold") && user2.getLastName().equals("Schwarzer"));
    }

    @Test
    @Order(6)
    public void testDeletePerson() {
        for (Person user : dummies) {
            testRestTemplate.delete(getRootUrl() + "/person/" + user.getId());
            try {
                getPersonByUsername(user.getEmailAddress());
            } catch (final HttpClientErrorException e) {
                assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
            }
        }
    }

    @Test
    @Order(7)
    public void testGetAllDummyPersonsButThereAreNon() throws IOException {
        try {
            testRestTemplate.getForObject(getRootUrl() + "/person/", Person[].class);
        } catch (HttpClientErrorException e) {
            assertNotNull(e.getResponseBodyAsByteArray());
            ErrorDetails details = new ObjectMapper().readValue(e.getResponseBodyAsByteArray(), ErrorDetails.class);
            assertEquals(ResourceNotFoundException.class.getSimpleName(), details.getException());
            assertEquals("No users initialized", details.getMessage());
        }
    }

    private Person getPersonByUsername(String username) {
        return testRestTemplate.getForObject(getRootUrl() + "/person/" + username, Person.class);
    }
}