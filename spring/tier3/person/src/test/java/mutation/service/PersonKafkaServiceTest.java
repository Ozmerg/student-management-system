package mutation.service;

import app.service.PersonKafkaService;
import app.service.PersonPersistenceService;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import general.dto.personEntities.Person;
import general.errorHandling.ResourceNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ActiveProfiles;

import java.util.Map;

import static org.assertj.core.api.Fail.fail;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles({"mutation"})
public class PersonKafkaServiceTest {

    @Mock
    private PersonPersistenceService personPersistenceService;

    @Mock
    private ObjectMapper objectMapper;

    @InjectMocks
    private PersonKafkaService personKafkaService;

    private ObjectMapper testMapper;

    private String personJson;

    private Person personObject;

    @BeforeEach
    public void setup() throws Exception {
        testMapper = new ObjectMapper();
        personObject = Person.builder().id(1L).build();
        personJson = testMapper.writeValueAsString(personObject);
    }


    @Test
    public void consume_Success() throws Exception {
        given(objectMapper.readValue(personJson, Person.class)).willReturn(personObject);
        given(personPersistenceService.deletePerson(1L)).willReturn(Map.of("Person deleted", true));
        try {
            personKafkaService.consume(personJson);
        } catch (Exception ex) {
            fail("This should not happen.");
        }
    }

    @Test
    public void consume_Fail_JsonException() throws Exception {
        String badSigJson = personJson + "badSig";
        given(objectMapper.readValue(badSigJson, Person.class)).willThrow(new JsonMappingException(null, "Something went wrong"));
        try {
            personKafkaService.consume(badSigJson);
        } catch (Exception ex) {
            fail("Exception handling happens within the consume method");
        }
    }

    @Test
    void consume_Fail_NotFound() throws Exception {
        given(objectMapper.readValue(personJson, Person.class)).willReturn(personObject);
        given(personPersistenceService.deletePerson(1L)).willThrow(new ResourceNotFoundException("Person not found for this id : " + personObject.getId()));
        try {
            personKafkaService.consume(personJson);
        } catch (Exception ex) {
            fail("Exception handling happens within the consume method. Not finding a 'to-be-deleted' entry is not a bad state but a worrying one");
        }
    }

}