package mutation.service;

import app.repository.PersonRepo;
import app.service.PersonPersistenceService;
import general.dto.personEntities.Person;
import general.errorHandling.ResourceNotFoundException;
import general.security.Roles;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ActiveProfiles;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Fail.fail;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles({"mutation"})
public class PersonPersistenceServiceTest {
    @Mock
    private PersonRepo personRepo;

    @Mock
    private EntityManager entityManager;

    @InjectMocks
    private PersonPersistenceService personPersistenceService;

    private final Person goodPerson = Person.builder().id(1L).creationDate(LocalDateTime.now())
            .birthDate(LocalDate.of(1995, 1, 1)).emailAddress("dummy@dummy.com").firstName("Dummy-dummy")
            .lastName("Dummy").role(Roles.STUDENT.name()).build();

    @Test
    public void getAllPersons_Success() {
        given(personRepo.findAll()).willReturn(List.of(goodPerson));
        assertTrue(personPersistenceService.getAllPersons().contains(goodPerson));
    }

    @Test
    public void getAllPersons_Fail_NotFound() {
        given(personRepo.findAll()).willReturn(null);
        try {
            personPersistenceService.getAllPersons();
            fail("At this point an exception should be thrown by getAllPerson()");
        } catch (ResourceNotFoundException ex) {
            assertTrue(ex.getMessage().equalsIgnoreCase("No users initialized"));
        }
    }

    @Test
    public void getPersonByEmail_Success() {
        given(personRepo.findByEmailAddress(goodPerson.getEmailAddress())).willReturn(List.of(goodPerson));
        assertTrue(personPersistenceService.getPersonByEmail(goodPerson.getEmailAddress()).equals(goodPerson));
    }

    @Test
    public void getPersonByEmail_Fail_NotFound() {
        given(personRepo.findByEmailAddress(goodPerson.getEmailAddress())).willReturn(Collections.emptyList());
        try {
            personPersistenceService.getPersonByEmail(goodPerson.getEmailAddress());
            fail("At this point an exception should be thrown by findByEmailAddress()");
        } catch (ResourceNotFoundException ex) {
            assertTrue(ex.getMessage().equalsIgnoreCase("There is no user with username: " + goodPerson.getEmailAddress()));
        }
    }

    @Test
    public void createPerson_Success() {
        given(personRepo.save(goodPerson)).willReturn(goodPerson);
        assertTrue(personPersistenceService.createPerson(goodPerson).equals(goodPerson));
    }

    @Test
    public void updatePerson_Success() {
        given(personRepo.findById(goodPerson.getId())).willReturn(Optional.of(goodPerson));
        given(personRepo.save(Mockito.any())).willReturn(goodPerson);
        assertTrue(personPersistenceService.updatePerson(goodPerson.getId(), goodPerson).equals(goodPerson));
    }

    @Test
    public void updatePerson_Fail_IdNotFound() {
        given(personRepo.findById(goodPerson.getId())).willThrow(new ResourceNotFoundException("Person not found for this id : " + goodPerson.getId()));
        try {
            personPersistenceService.updatePerson(goodPerson.getId(), goodPerson);
            fail("The test should fail by now, because an ResourceNotFoundException should be thrown by private method findPerson()");
        } catch (ResourceNotFoundException ex) {
            assertTrue(ex.getMessage().equalsIgnoreCase("Person not found for this id : " + goodPerson.getId()));
        }
    }

    @Test
    public void deletePerson_Success() {
        given(personRepo.findById(goodPerson.getId())).willReturn(Optional.of(goodPerson));
        assertTrue(personPersistenceService.deletePerson(goodPerson.getId()).equals(Map.of("Person deleted", true)));
    }

    @Test
    public void deletePerson_Fail_PersonNotFound() {
        given(personRepo.findById(goodPerson.getId())).willThrow(new ResourceNotFoundException("Person not found for this id : " + goodPerson.getId()));
        try {
            personPersistenceService.deletePerson(goodPerson.getId());
            fail("This test should fail at this point because an ResourceNotFoundException should be thrown by private method findPerson()");
        } catch (ResourceNotFoundException ex) {
            assertTrue(ex.getMessage().equalsIgnoreCase("Person not found for this id : " + goodPerson.getId()));
        }
    }
}