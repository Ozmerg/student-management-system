package mutation.controller;

import app.controller.PersonController;
import app.service.PersonPersistenceService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import general.dto.personEntities.Person;
import general.errorHandling.ErrorDetails;
import general.errorHandling.GlobalExceptionHandler;
import general.errorHandling.ResourceNotFoundException;
import general.security.Roles;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles({ "mutation" })
public class PersonControllerTest {

        private MockMvc mvc;

        private ObjectMapper mapper;

        @Mock
        private PersonPersistenceService personPersistenceService;

        @InjectMocks
        private PersonController personController;

        private final Person goodPerson = Person.builder().id(1L).creationDate(LocalDateTime.now())
                .birthDate(LocalDate.of(1995, 1, 1)).emailAddress("dummy@dummy.com").firstName("Dummy-dummy")
                        .lastName("Dummy").role(Roles.STUDENT.name()).build();

        @BeforeEach
        public void setup() {
                mvc = MockMvcBuilders.standaloneSetup(personController)
                                .setControllerAdvice(new GlobalExceptionHandler()).build();
                mapper = new ObjectMapper();
                mapper.registerModule(new JavaTimeModule());
        }

        @Test
        public void getAllPersons_Success() throws Exception {
                given(personPersistenceService.getAllPersons())
                                .willReturn(Stream.of(goodPerson).collect(Collectors.toList()));
                MockHttpServletResponse response = mvc.perform(get("/person").accept(MediaType.APPLICATION_JSON))
                                .andReturn().getResponse();
                assertEquals(HttpStatus.OK.value(), response.getStatus());
                List<Person> list = mapper.readValue(response.getContentAsString(), new TypeReference<List<Person>>() {
                });
                assertEquals("Dummy-dummy", list.get(0).getFirstName());
        }

        @Test
        public void getAllPerson_Fail() throws Exception {
                given(personPersistenceService.getAllPersons())
                                .willThrow(new ResourceNotFoundException("No users initialized"));
                MockHttpServletResponse response = mvc.perform(get("/person").accept(MediaType.APPLICATION_JSON))
                                .andReturn().getResponse();
                assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
                assertEquals("No users initialized",
                                mapper.readValue(response.getContentAsString(), ErrorDetails.class).getMessage());
        }

        @Test
        public void createPerson_Success() throws Exception {
                given(personPersistenceService.createPerson(goodPerson)).willReturn(goodPerson);
                MockHttpServletResponse response = mvc.perform(post("/person")
                                .content(mapper.writeValueAsString(goodPerson)).contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();
                assertEquals(HttpStatus.OK.value(), response.getStatus());
                assertEquals(goodPerson.getFirstName(),
                                mapper.readValue(response.getContentAsString(), Person.class).getFirstName());
        }

        @Test
        public void createPerson_Fail() throws Exception {
                MockHttpServletResponse response = mvc.perform(post("/person", "").accept(MediaType.APPLICATION_JSON))
                                .andReturn().getResponse();
                assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        }

        @Test
        public void getPersonByEmail_Success() throws Exception {
                given(personPersistenceService.getPersonByEmail(Mockito.anyString())).willReturn(goodPerson);
                MockHttpServletResponse response = mvc.perform(
                                get("/person/" + goodPerson.getEmailAddress()).accept(MediaType.APPLICATION_JSON))
                                .andReturn().getResponse();
                assertEquals(HttpStatus.OK.value(), response.getStatus());
                assertTrue(goodPerson.equals(mapper.readValue(response.getContentAsString(), Person.class)),
                                "Maybe the equals function is not working properly");
        }

        @Test
        public void getPersonByEmail_Fail() throws Exception {
                final String email = "badEmailAdress";
                given(personPersistenceService.getPersonByEmail(email))
                                .willThrow(new ResourceNotFoundException("There is no user with username: " + email));
                MockHttpServletResponse response = mvc
                                .perform(get("/person/" + email).accept(MediaType.APPLICATION_JSON)).andReturn()
                                .getResponse();
                assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
                assertEquals("There is no user with username: " + email,
                                mapper.readValue(response.getContentAsString(), ErrorDetails.class).getMessage());
        }

        @Test
        public void updatePerson_Success() throws Exception {
                Person details4Change = Person.builder().birthDate(LocalDate.of(1995, 1, 1))
                        .emailAddress("dummy@dummies.com").firstName("Dummy-dummyies").lastName("Dummies")
                        .role(Roles.STUDENT.name()).build();
                given(personPersistenceService.updatePerson(1L, details4Change))
                        .willReturn(details4Change);
                MockHttpServletResponse response = mvc.perform(put("/person/" + 1)
                        .content(mapper.writeValueAsString(details4Change))
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andReturn()
                        .getResponse();
                assertEquals(HttpStatus.OK.value(), response.getStatus());
                assertEquals(details4Change.getFirstName(),
                        mapper.readValue(response.getContentAsString(), Person.class).getFirstName());
        }

        @Test
        public void updatePerson_Fail_WrongId() throws Exception {
                Person details4Change = Person.builder().birthDate(LocalDate.of(1995, 1, 1))
                        .emailAddress("dummy@dummies.com").firstName("Dummy-dummies").lastName("Dummies")
                        .role(Roles.STUDENT.name()).build();
                given(personPersistenceService.updatePerson(2L, details4Change))
                        .willThrow(new ResourceNotFoundException("Person not found for this id : 2"));
                MockHttpServletResponse response = mvc.perform(put("/person/" + 2)
                        .content(mapper.writeValueAsString(details4Change))
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andReturn()
                        .getResponse();
                assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
                assertEquals("Person not found for this id : 2",
                        mapper.readValue(response.getContentAsString(), ErrorDetails.class).getMessage());
        }

        @Test
        public void updatePerson_Fail_InvalidChangeDetails() throws Exception {
                Person details4Change = Person.builder().birthDate(LocalDate.of(1995, 1, 1))
                        .emailAddress("invalid.com").firstName("i2").lastName("a").role(Roles.STUDENT.name())
                        .build();
                MockHttpServletResponse response = mvc.perform(put("/person/" + 1)
                        .content(mapper.writeValueAsString(details4Change))
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andReturn()
                        .getResponse();
                assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
                ErrorDetails readValue = mapper.readValue(response.getContentAsString(), ErrorDetails.class);
                assertEquals(MethodArgumentNotValidException.class.getSimpleName(), readValue.getException());
        }

        @Test
        public void deletePerson_Success() throws Exception {
                Map<String, Boolean> expectedMap = Map.of("Person deleted", true);
                given(personPersistenceService.deletePerson(1L)).willReturn(expectedMap);
                MockHttpServletResponse response = mvc
                                .perform(delete("/person/" + 1).accept(MediaType.APPLICATION_JSON)).andReturn()
                                .getResponse();
                assertEquals(HttpStatus.OK.value(), response.getStatus());
                assertTrue(mapper.readValue(response.getContentAsString(), Map.class).equals(expectedMap),
                                "The returned result Map is no longer equal with the expected one. Maybe the describtion text changed?");
        }

        @Test
        public void deletePerson_Failed_IdNotFound() throws Exception {
                String expectedMsg = "Person not found for this id : " + 2L;
                given(personPersistenceService.deletePerson(2L)).willThrow(new ResourceNotFoundException(expectedMsg));
                MockHttpServletResponse response = mvc
                                .perform(delete("/person/" + 2).accept(MediaType.APPLICATION_JSON)).andReturn()
                                .getResponse();
                assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
                assertEquals(expectedMsg,
                                mapper.readValue(response.getContentAsString(), ErrorDetails.class).getMessage());
        }
}