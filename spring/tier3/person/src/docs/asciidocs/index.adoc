= Todo API Documentation

== Introduction
This is the documentation of the Person RESTful service.

== Person API
Collection of CRUD API endpoints used to manipulate Todo entries in the application.

=== Get Person By Id
Obtains a specific Todo registered in the application by its unique identifier.

==== Path Parameters
include::{snippets}/get-todo-by-id/path-parameters.adoc[]

==== Sample Request
include::{snippets}/get-todo-by-id/http-request.adoc[]

==== Sample Response
include::{snippets}/get-todo-by-id/http-response.adoc[]

==== CURL sample
include::{snippets}/get-todo-by-id/curl-request.adoc[]


=== Get all Person
Obtains all Person registered in the application.

==== Sample Request
include::{snippets}/get-all-todos/http-request.adoc[]

==== Sample Response
include::{snippets}/get-all-todos/http-response.adoc[]

==== CURL sample
include::{snippets}/get-all-todos/curl-request.adoc[]


=== Delete Person By Id
Deletes a specific Person registered in the application by its unique identifier.

==== Path Parameters
include::{snippets}/delete-todo/path-parameters.adoc[]

==== Sample Request
include::{snippets}/delete-todo/http-request.adoc[]

==== Sample Response
include::{snippets}/delete-todo/http-response.adoc[]

==== CURL sample
include::{snippets}/delete-todo/curl-request.adoc[]


=== Create Person 
Creates the provided Person in the application.

==== Sample Request Fields
include::{snippets}/create-todo/request-fields.adoc[]

==== Sample Request
include::{snippets}/create-todo/http-request.adoc[]

==== Sample Response
include::{snippets}/create-todo/http-response.adoc[]

==== CURL sample
include::{snippets}/create-todo/curl-request.adoc[]