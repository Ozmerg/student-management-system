package app.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import general.dto.personEntities.Person;
import general.errorHandling.ResourceNotFoundException;
import general.kafka.KafkaTopicConstants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class PersonKafkaService {

    private final PersonPersistenceService personPersistenceService;

    private final ObjectMapper objectMapper;

    @KafkaListener(topics = KafkaTopicConstants.AUTHENTICATION_FAILED, groupId = KafkaTopicConstants.AUTHENTICATION_FAILED_PERSON_GROUP_ID)
    public void consume(String message) {
        log.debug(String.format("$$$$ => Consumed message: %s", message));
        try {
            Person rollback = objectMapper.readValue(message, Person.class);
            personPersistenceService.deletePerson(rollback.getId());
        } catch (JsonProcessingException e) {
            log.error(String.format(
                    "The given message in topic %s can't be mapped to Person.class. See further details: %s",
                    KafkaTopicConstants.AUTHENTICATION_FAILED, e.getMessage()));
        } catch (ResourceNotFoundException e) {
            log.debug(String.format(
                    "The provided person can't be found in db. This is ok because of the given rollback situation. Some details: %s",
                    e.getMessage()));
        }
    }
}