package app.service;

import app.repository.PersonRepo;
import general.dto.personEntities.Person;
import general.errorHandling.ResourceNotFoundException;
import io.jsonwebtoken.lang.Collections;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class PersonPersistenceService {

    private final PersonRepo personRepo;

    private final EntityManager entityManager;

    public List<Person> getAllPersons() throws ResourceNotFoundException {
        List<Person> list = personRepo.findAll();
        if (Collections.isEmpty(list)) {
            throw new ResourceNotFoundException("No users initialized");
        }
        return list;
    }

    public Person getPersonByEmail(String email) throws ResourceNotFoundException {
        final String finalPersonEmail = StringUtils.trimAllWhitespace(email).toLowerCase(Locale.ROOT);
        Person dbPerson = personRepo.findByEmailAddress(finalPersonEmail).stream().findFirst().orElseThrow(
                () -> new ResourceNotFoundException("There is no user with username: " + finalPersonEmail));
        entityManager.detach(dbPerson);
        return dbPerson;
    }

    public Person createPerson(Person person) {
        person.setEmailAddress(StringUtils.trimAllWhitespace(person.getEmailAddress()).toLowerCase(Locale.ROOT));
        Person dbPerson = personRepo.save(person);
        entityManager.detach(dbPerson);
        return dbPerson;
    }

    public Person updatePerson(Long personId, Person personDetails)
            throws ResourceNotFoundException {
        Person dbPerson = findPerson(personId);
        Person updatedPerson = Person.builder().id(dbPerson.getId()).creationDate(dbPerson.getCreationDate())
                .birthDate(personDetails.getBirthDate()).emailAddress(personDetails.getEmailAddress())
                .firstName(personDetails.getFirstName()).lastName(personDetails.getLastName())
                .role(personDetails.getRole().toUpperCase(Locale.ROOT)).build();
        Person returnedDbPerson = personRepo.save(updatedPerson);
        entityManager.detach(returnedDbPerson);
        return returnedDbPerson;
    }

    public Map<String, Boolean> deletePerson(Long personId) throws ResourceNotFoundException {
        personRepo.delete(findPerson(personId));
        return Map.of("Person deleted", true);
    }

    private Person findPerson(Long personId) {
        return personRepo.findById(personId)
                .orElseThrow(() -> new ResourceNotFoundException("Person not found for this id : " + personId));
    }
}