package app;

import java.util.Locale;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;

import org.apache.http.ssl.SSLContextBuilder;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import general.dto.personEntities.Person;
import general.kafka.KafkaTopicConstants;
import general.security.EurekaHostnameVerifier;

@SpringBootApplication
@EnableKafka
@EnableEurekaClient
@EnableJpaRepositories
@EntityScan(basePackageClasses = Person.class)
public class PersonApplication {

	@Value("${server.ssl.trust-store-password}")
	private String trustStoreKey;

	public static void main(String[] args) {
		SpringApplication.run(PersonApplication.class, args);
	}

	@Bean
	public LocaleResolver localeResolver() {
		SessionLocaleResolver slr = new SessionLocaleResolver();
		slr.setDefaultLocale(Locale.US);
		return slr;
	}

	@Bean
	public SSLContext sslContext() throws Exception {
		return new SSLContextBuilder()
				.loadTrustMaterial(ResourceUtils.getFile("classpath:x509/persontruststore.p12"),
						trustStoreKey.toCharArray())
				.loadKeyMaterial(ResourceUtils.getFile("classpath:x509/person.p12"), trustStoreKey.toCharArray(),
						trustStoreKey.toCharArray())
				.build();
	}

	@Bean
	public HostnameVerifier getEurekaHostnameVerifier() {
		return new EurekaHostnameVerifier();
	}

	@Bean
	public NewTopic topic1() {
		return TopicBuilder.name(KafkaTopicConstants.AUTHENTICATION_FAILED).replicas(3).partitions(3).compact().build();
	}
}
