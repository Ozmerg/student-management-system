package app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import general.dto.personEntities.Person;

@Repository("PersonRepo")
public interface PersonRepo extends JpaRepository<Person, Long> {

    List<Person> findByEmailAddress(String emailAddress);
}
