package app.controller;

import app.service.PersonPersistenceService;
import general.dto.personEntities.Person;
import general.errorHandling.GlobalExceptionHandler;
import general.errorHandling.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

@RestController("PersonController")
@RequiredArgsConstructor
public class PersonController extends GlobalExceptionHandler {

    private PersonPersistenceService personPersistenceService;

    @GetMapping("/person")
    public List<Person> getAllPersons() throws ResourceNotFoundException {
        return personPersistenceService.getAllPersons();
    }

    @GetMapping("/person/{email}")
    public Person getPersonByEmail(@PathVariable @NotBlank String email) throws ResourceNotFoundException {
        return personPersistenceService.getPersonByEmail(email);
    }

    @PostMapping("/person")
    public Person createPerson(@Validated @RequestBody Person person) throws MethodArgumentNotValidException {
        return personPersistenceService.createPerson(person);
    }

    @PutMapping("/person/{id}")
    public Person updatePerson(@PathVariable Long id, @Validated @RequestBody Person personDetails)
            throws ResourceNotFoundException, MethodArgumentNotValidException {
        return personPersistenceService.updatePerson(id, personDetails);
    }

    @DeleteMapping("/person/{id}")
    public Map<String, Boolean> deletePerson(@PathVariable Long id) throws ResourceNotFoundException {
        return personPersistenceService.deletePerson(id);
    }
}