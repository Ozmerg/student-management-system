package services.db.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.web.client.HttpClientErrorException;

import app.PwdApplication;
import app.services.db.api.PwdRepo;
import general.dto.pwdEntities.Pwd;
import general.errorHandling.ErrorDetails;
import general.errorHandling.ResourceNotFoundException;

@SpringJUnitConfig
@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
@SpringBootTest(classes = PwdApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PwdControllerTest {

	private static TestRestTemplate testRestTemplate;

	@LocalServerPort
	private int port;

	@Autowired
	private PwdRepo pwdRepo;

	private String getRootUrl() {
		return "http://localhost:" + port;
	}

	private static List<Pwd> badDummies;

	private static List<Pwd> goodDummies;

	private static Pwd notfound;

	Logger logger = LoggerFactory.getLogger(PwdControllerTest.class);

	@BeforeAll
	public static void setUp() {
		testRestTemplate = new TestRestTemplate();
		testRestTemplate.getRestTemplate()
				.setErrorHandler(new org.springframework.web.client.DefaultResponseErrorHandler());
		badDummies = List.of(Pwd.builder().fkPerson(Long.valueOf(Long.MAX_VALUE - 4)).prePwd("12345").build(),
				Pwd.builder().fkPerson(Long.valueOf(Long.MAX_VALUE - 3)).prePwd("91822ssmqosndKNQONSOQ").build());
		goodDummies = List.of(
				Pwd.builder().fkPerson(Long.valueOf(Long.MAX_VALUE - 2)).prePwd("H!FKW§63ÄCsk(qs2ks1").build(),
				Pwd.builder().fkPerson(Long.valueOf(Long.MAX_VALUE - 1)).prePwd("j3%l0A!K4$a=8M").build());
		notfound = Pwd.builder().fkPerson(Long.valueOf(Long.MAX_VALUE)).prePwd("%§sj1qJq2I81ks13").build();
	}

	@Test
	@Order(1)
	public void testCreatePwd() {
		List<Pwd> joinedDummies = Stream.concat(badDummies.stream(), goodDummies.stream()).collect(Collectors.toList());
		for (Pwd dummy : joinedDummies) {
			try {
				testRestTemplate.postForObject(getRootUrl() + "/pwd/", dummy, Pwd.class);
			} catch (HttpClientErrorException e) {
				assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
				logger.error(e.getMessage());
			}
		}
		assertTrue(pwdRepo.findAll().size() == goodDummies.size(),
				goodDummies.size() + " dummy pwd entries should be created by now");
	}

	@Test
	@Order(2)
	public void testgetPwd() throws IOException {
		try {
			for (Pwd pwd : goodDummies) {
				testRestTemplate.getForObject(getRootUrl() + "/pwd/" + pwd.getFkPerson(), Pwd.class);
			}
		} catch (HttpClientErrorException e) {
			fail("This should not happen because the good dummies are already in the db");
		}
		try {
			for (Pwd pwd : badDummies) {
				testRestTemplate.getForObject(getRootUrl() + "/pwd/" + pwd.getFkPerson(), Pwd.class);
				fail("This line should not be reached due to an expected ResourceNotFoundException");
			}
		} catch (HttpClientErrorException e) {
			assertNotNull(e);
			assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
			ErrorDetails details = new ObjectMapper().readValue(e.getResponseBodyAsString(), ErrorDetails.class);
			assertEquals(ResourceNotFoundException.class.getSimpleName(), details.getException());
		}
	}

	@Test
	@Order(3)
	public void testPutPwd() {
		try {
			testRestTemplate.put(getRootUrl() + "/pwd/", notfound);
			fail("The provided Pwd is not present in the db and should thereby return an HttpResponse<ResourceNotFoundException>");
		} catch (HttpClientErrorException e) {
			assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
		}
		Pwd goodDummy = goodDummies.get(0);
		goodDummy.setPrePwd("1234asdf(");
		try {
			testRestTemplate.put(getRootUrl() + "/pwd/", goodDummy);
			fail("The provided password is too weak and should be rejected");
		} catch (HttpClientErrorException e) {
			assertEquals(e.getStatusCode(), HttpStatus.BAD_REQUEST);
		}
		goodDummy.setPrePwd("F=ö1Nai%aq9rq1N(");
		try {
			testRestTemplate.put(getRootUrl() + "/pwd/", goodDummy);
		} catch (HttpClientErrorException e) {
			fail("This line of code is not allowed to be executed. The goodDummy should be able to change it's pwd without flaws");
		}
	}

	@Test
	@Order(4)
	public void testDeletePwd() {
		for (Pwd dummy : goodDummies) {
			try {
				testRestTemplate.delete(getRootUrl() + "/pwd/" + dummy.getFkPerson());
			} catch (HttpClientErrorException e) {
				logger.error(e.getMessage());
			}
		}
		assertTrue(pwdRepo.findAll().isEmpty(), "All dummy pwds should be deleted by now");
	}
}
