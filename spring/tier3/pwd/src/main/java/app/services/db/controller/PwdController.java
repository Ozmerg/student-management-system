package app.services.db.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import app.services.db.api.PwdRepo;
import general.dto.pwdEntities.Pwd;
import general.errorHandling.GlobalExceptionHandler;
import general.errorHandling.ResourceNotFoundException;

@RestController
public class PwdController extends GlobalExceptionHandler {

    @Autowired
    private PwdRepo pwdRepo;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @GetMapping("/pwd/{fk_person}")
    public Pwd getPwd(@PathVariable(value = "fk_person") Long fkId) throws ResourceNotFoundException {
        return findByFkPerson(fkId);
    }

    @PostMapping("/pwd")
    public Pwd createPwd(@Validated @RequestBody Pwd pwd) {
        pwd.setHashedPwd(encoder.encode(pwd.getPrePwd()));
        return pwdRepo.save(pwd);
    }

    @PutMapping("/pwd")
    public Pwd updatePwd(@Validated @RequestBody Pwd pwdDetails) throws ResourceNotFoundException {
        Pwd dbPwd = findByFkPerson(pwdDetails.getFkPerson());
        dbPwd.setPrePwd(pwdDetails.getPrePwd());
        dbPwd.setHashedPwd(encoder.encode(pwdDetails.getPrePwd()));
        return pwdRepo.save(dbPwd);
    }

    @DeleteMapping("/pwd/{fk_person}")
    public Map<String, Boolean> deletePwd(@PathVariable(value = "fk_person") Long fkId)
            throws ResourceNotFoundException {
        Pwd pwd = findByFkPerson(fkId);
        pwdRepo.delete(pwd);
        return Map.of("pwd deleted", true);
    }

    private Pwd findByFkPerson(Long fkId) throws ResourceNotFoundException {
        return pwdRepo.findByFkPerson(fkId).stream().findFirst()
                .orElseThrow(() -> new ResourceNotFoundException("There is no pwd with fk_person: " + fkId));
    }
}
