package app.services.db.api;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import general.dto.pwdEntities.Pwd;

@Repository
public interface PwdRepo extends JpaRepository<Pwd, Long> {

    public List<Pwd> findByFkPerson(Long id);
}
