package general.dto.operationRequests;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import general.dto.personEntities.Person;
import general.dto.pwdEntities.Pwd;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

/**
 * This is a helper class for processing the full user details in the register
 * operation
 */
@Getter
@AllArgsConstructor
@NotNull
@Builder
@JsonAutoDetect
public class RegisterRequest {

    @NotNull
    @JsonProperty("Person")
    private Person person;
    @NotNull
    @JsonProperty("Pwd")
    private Pwd pwd;

}
