package general.dto.personEntities;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import general.security.Roles;
import general.validation.enumeration.ValueOfEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity()
@Table(name = "person")
public class Person implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final String namingPattern = "^(?!\\s)[a-zA-ZÄäÖöÜüß\\-]{2,128}$";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Generated(GenerationTime.INSERT)
    @Column(name = "creation_date", nullable = false, insertable = false)
    private LocalDateTime creationDate;

    @NotBlank
    @Pattern(regexp = namingPattern, message = "firstname must match the regex: " + namingPattern)
    @Size(min = 2, max = 128, message = "the persons firstname ${validatedValue} must be between {min} and {max} characters long")
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @NotBlank
    @Pattern(regexp = namingPattern, message = "lastname must match the regex: " + namingPattern)
    @Size(min = 2, max = 128, message = "the persons lastname ${validatedValue} must be between {min} and {max} characters long")
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @NotNull
    @Past
    @Column(name = "birth_date", nullable = false)
    private LocalDate birthDate;

    @NotBlank
    @Email
    @Size(max = 128, message = "the persons email ${validatedValue} must be max {max} characters long")
    @Column(name = "email", nullable = false)
    private String emailAddress;

    @NotBlank
    @ValueOfEnum(enumClass = Roles.class)
    @Column(name = "role", nullable = false)
    private String role;
}