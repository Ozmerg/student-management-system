package general.security;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@Component
public class JwtConfig {

    @Value("${security.jwt.uri:/auth,/auth/register}")
    private String[] uriStrings;

    @Value("${security.jwt.header:Authorization}")
    private String header;

    @Value("${security.jwt.prefix:SMSToken_}")
    private String prefix;

    @Value("${security.jwt.issuer:Gismo}")
    private String issuer;

    @Value("${security.jwt.expiration:#{60*60*1000}}")
    private int expiration;

    @Value("${security.jwt.secret:k8!x3k&bä$xrh§1X52xQZ}")
    private String secret;

    @Value("${security.jwt.refreshExpiration:#{2*60*60*1000}}")
    private int refreshExpiration;

    /**
     * <i>Notes: While using {@link io.jsonwebtoken.JwtBuilder JwtBuilder} it is
     * highly recommended to put the
     * {@link io.jsonwebtoken.JwtBuilder#setClaims(Map) claims} at the first
     * position of the stream. If not, the {@link io.jsonwebtoken.JwtBuilder
     * JwtBuilder} might not recognise and disgard a property set before the claims
     * </i>
     * 
     * @param claims          must be of type {@link java.util.List List}
     * @param subject         name of the user, typically an unique email address or
     *                        username
     * @param withRefreshTime determens if the refresh or the default experation
     *                        time should be set for the jwt expiration time
     * @return new jwt token
     */
    public String doGenerateToken(Map<String, Object> claims, String subject, boolean withRefreshTime) {
        Long now = System.currentTimeMillis();
        return prefix + Jwts.builder().setClaims(claims).setSubject(subject).setIssuer(this.getIssuer())
                .setExpiration(new Date(now + (withRefreshTime ? this.getRefreshExpiration() : this.getExpiration())))
                .setIssuedAt(new Date(now)).signWith(SignatureAlgorithm.HS256, this.getSecret().getBytes()).compact();
    }

    /**
     * Convenience method for token validation
     * 
     * @param token
     * @return whether a given token has a valid syntax or not
     */
    public boolean validateToken(String token) {
        return Jwts.parser().isSigned(token.replace(prefix, ""));
    }
}
