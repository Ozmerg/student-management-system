package general.security;

import lombok.RequiredArgsConstructor;
import org.apache.http.conn.util.InetAddressUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Component;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.cert.Certificate;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Component
@RequiredArgsConstructor
public class EurekaHostnameVerifier implements HostnameVerifier {

    private DiscoveryClient discoveryClient;

    Logger logger = LoggerFactory.getLogger(EurekaHostnameVerifier.class);

    private static final String EUREKA_KEY_WORD = "service";

    @Override
    public boolean verify(String hostname, SSLSession session) {
        try {
            final Certificate[] certs = session.getPeerCertificates();
            final X509Certificate x509 = (X509Certificate) certs[0];
            final List<String> subjectAlts = getSubjectAltNames(x509);
            if (logger.isDebugEnabled()) {
                logger.debug(String.format("The processed host ip's are: %s", subjectAlts.toString()));
            }
            for (String althostname : subjectAlts) {
                if (althostname.equals(hostname)) {
                    return true;
                }
            }
            return false;
        } catch (SSLPeerUnverifiedException e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    private List<String> getSubjectAltNames(X509Certificate crt) {
        try {
            Collection<List<?>> entries = crt.getSubjectAlternativeNames();
            final List<String> altNamesForLookup = new ArrayList<>();
            for (List<?> entry : entries) {
                final Object item = entry.get(1);
                if (item instanceof String) {
                    altNamesForLookup.add((String) item);
                }
            }
            if (logger.isDebugEnabled()) {
                logger.debug(String.format("The pre-processed host addresses are: %s", altNamesForLookup.toString()));
            }
            return searchDnsInRegistry(altNamesForLookup);
        } catch (CertificateParsingException e) {
            logger.error("Couldn't parse crt alt names", e);
            return Collections.emptyList();
        }
    }

    private List<String> searchDnsInRegistry(List<String> altNamesForLookup) {
        final List<String> finalNames = new ArrayList<>();
        for (String preName : altNamesForLookup) {
            if (preName.contains(EUREKA_KEY_WORD)) {
                for (ServiceInstance instance : discoveryClient.getInstances(preName)) {
                    finalNames.add(instance.getHost());
                }
            } else {
                finalNames.add(normaliseAddress(preName));
            }
        }
        return finalNames;
    }

    private String normaliseAddress(String preName) {
        if (!InetAddressUtils.isIPv4Address(preName)) {
            try {
                preName = InetAddress.getByName(preName).getHostAddress();
            } catch (final UnknownHostException unexpected) {
                logger.error(unexpected.getMessage());
            }
        }
        return preName;
    }

}