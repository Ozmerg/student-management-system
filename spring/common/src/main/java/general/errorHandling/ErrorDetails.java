package general.errorHandling;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorDetails {

    @Builder.Default
    private String dateTime = DateTimeFormatter.ISO_ZONED_DATE_TIME.format(ZonedDateTime.now());

    private String message;

    private String path;

    private String exception;
}