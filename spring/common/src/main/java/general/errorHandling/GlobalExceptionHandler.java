package general.errorHandling;

import javax.naming.ServiceUnavailableException;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice("GlobalExceptionHandler")
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Object> resourceNotFoundExc(ResourceNotFoundException ex, WebRequest request) {
        return createErrorResponse(ex, request, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<Object> tegrityViolationExc(DataIntegrityViolationException ex, WebRequest request) {
        return createErrorResponse(ex, request, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        return createErrorResponse(ex, request, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> illegalArgumentExc(IllegalArgumentException ex, WebRequest request) {
        return createErrorResponse(ex, request, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpClientErrorException.class)
    public ResponseEntity<Object> httpClientErrorExc(HttpClientErrorException ex, WebRequest request) {
        return createErrorResponse(ex, request, ex.getStatusCode());
    }

    @ExceptionHandler(ServiceUnavailableException.class)
    public ResponseEntity<Object> serviceUnavailableExc(ServiceUnavailableException ex, WebRequest request) {
        return createErrorResponse(ex, request, HttpStatus.SERVICE_UNAVAILABLE);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        StringBuilder builder = new StringBuilder();
        int count = 1;
        for (FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
            builder.append(count++ + ". " + fieldError.getDefaultMessage() + "\n");
        }
        return new ResponseEntity<Object>(ErrorDetails.builder()
                .message(
                        String.format("There are %s field error(s):\n%s", ex.getAllErrors().size(), builder.toString()))
                .path(request.getDescription(false)).exception(ex.getClass().getSimpleName()).build(), headers, status);
    }

    private ResponseEntity<Object> createErrorResponse(Throwable ex, WebRequest request, HttpStatus status) {
        return new ResponseEntity<>(
                ErrorDetails.builder().message(ex == null ? "Unknown exception occured" : ex.getMessage())
                        .path(request == null ? "Unknown path" : request.getDescription(false))
                        .exception(ex == null ? "Unknown exception name" : ex.getClass().getSimpleName()).build(),
                status == null ? HttpStatus.INTERNAL_SERVER_ERROR : status);
    }
}