package general.validation.pwd;

import java.util.Arrays;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.google.common.base.Joiner;

import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.GermanCharacterData;
import org.passay.LengthRule;
import org.passay.PasswordData;
import org.passay.PasswordValidator;
import org.passay.RuleResult;
import org.passay.WhitespaceRule;

public class PasswordConstraintValidator implements ConstraintValidator<StrongPassword, String> {

    @Override
    public void initialize(StrongPassword arg0) {
    }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        PasswordValidator validator = new PasswordValidator(Arrays.asList(new LengthRule(12, 50), new WhitespaceRule(),
                new CharacterRule(GermanCharacterData.LowerCase, 2),
                new CharacterRule(GermanCharacterData.UpperCase, 2), new CharacterRule(EnglishCharacterData.Digit, 3),
                new CharacterRule(EnglishCharacterData.Special, 2)));

        RuleResult result = validator.validate(new PasswordData(password));
        if (result.isValid()) {
            return true;
        }
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(Joiner.on(",").join(validator.getMessages(result)))
                .addConstraintViolation();
        return false;
    }
}
