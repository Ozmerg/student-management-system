package general.kafka;

public final class KafkaTopicConstants {

    public static final String AUTHENTICATION_FAILED = "Person_RollBack";
    public static final String AUTHENTICATION_FAILED_PERSON_GROUP_ID = "PersonConsumerGroup";
}
