package general.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
public class KafkaProducerService {
    private static final Logger logger = LoggerFactory.getLogger(KafkaProducerService.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void sendMessage(String topic, String message) {
        logger.info(String.format("Topic -> %s, Message sent -> %s", topic, message));
        ListenableFuture<SendResult<String, String>> future = this.kafkaTemplate.send(topic, "SmsApplicationKey",
                message);
        future.addCallback(new ListenableFutureCallback<>() {
            @Override
            public void onFailure(Throwable ex) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Unable to send message=[ {} ] due to : {}", message, ex.getMessage());
                }
            }

            @Override
            public void onSuccess(SendResult<String, String> result) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Sent message=[ {} ] with offset=[ {} ]", message,
                            result.getRecordMetadata().offset());
                }
            }
        });
    }
}
