package app;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cloud.netflix.eureka.http.RestTemplateEurekaHttpClient;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;

import general.security.EurekaHostnameVerifier;

@EnableEurekaServer
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class,
		KafkaAutoConfiguration.class })
public class EurekaApplication {

	@Value("${server.ssl.trust-store-password}")
	private String trustStoreKey;

	@Value("${eureka.client.serviceUrl.defaultZone}")
	private String serviceUrl;

	public static void main(String[] args) {
		SpringApplication.run(EurekaApplication.class, args);
	}

	@Bean
	public SSLContext sslContext() throws Exception {
		return new SSLContextBuilder()
				.loadTrustMaterial(ResourceUtils.getFile("classpath:x509/eurekatruststore.p12"),
						trustStoreKey.toCharArray())
				.loadKeyMaterial(ResourceUtils.getFile("classpath:x509/eureka.p12"), trustStoreKey.toCharArray(),
						trustStoreKey.toCharArray())
				.build();
	}

	@Bean
	public CloseableHttpClient getCloseableHttpClient() throws Exception {
		SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(sslContext());
		return HttpClients.custom().setSSLSocketFactory(socketFactory).build();
	}

	@Bean
	public HostnameVerifier getEurekaHostnameVerifier() {
		return new EurekaHostnameVerifier();
	}

	@Bean
	public RestTemplateEurekaHttpClient getEurekaRestTemplate(RestTemplate restTemplate) {
		return new RestTemplateEurekaHttpClient(restTemplate, serviceUrl);
	}

	@Bean
	public RestTemplate getRestTemplate(CloseableHttpClient closeableHttpClient) throws Exception {
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(
				closeableHttpClient);
		return new RestTemplate(factory);
	}

}
