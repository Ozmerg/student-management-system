package app.security;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import general.security.JwtConfig;

public class JwtUsernameAndPasswordAuthFilter extends UsernamePasswordAuthenticationFilter
        implements AuthenticationFailureHandler {

    private AuthenticationManager authManager;

    private final JwtConfig jwtConfig;

    private List<String> creds;

    private static final String LOGIN_PATH = "/auth";

    Logger LOG = LoggerFactory.getLogger(JwtUsernameAndPasswordAuthFilter.class);

    public JwtUsernameAndPasswordAuthFilter(AuthenticationManager authManager, JwtConfig jwtConfig) {
        this.authManager = authManager;
        this.jwtConfig = jwtConfig;

        // Override, because default path is /login, to /auth
        this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(LOGIN_PATH, "POST"));
        this.setFilterProcessesUrl(LOGIN_PATH);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {

        try {
            creds = new ObjectMapper().readValue(request.getInputStream(), new TypeReference<List<String>>() {
            });

            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(creds.get(0),
                    creds.get(1), Collections.emptyList());

            if (LOG.isDebugEnabled())
                LOG.debug("Try to authenticate user: " + creds.get(0));
            return authManager.authenticate(authToken);

        } catch (DisabledException e) {
            LOG.error("The requested users account was disabled. Please contact support for further instructions", e);
            throw new DisabledException("USER_DISABLED");
        } catch (BadCredentialsException e) {
            LOG.error("The provided creds are not valid for user:" + creds.get(0));
            throw new BadCredentialsException("BAD_CREDS");
        } catch (LockedException e) {
            LOG.error("The requested users account is locked. Please contact support for furhter instructions", e);
            throw new LockedException("USER_LOCKED");
        } catch (IOException e) {
            LOG.error("An IOException was raised by the ObjectMapper during authentication."
                    + " Please make sure that you send a json list object with username and password", e);
            throw new RuntimeException("BAD_JSON_FORM");
        }
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException exception) throws IOException, ServletException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), exception.getLocalizedMessage());
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException failed) throws IOException, ServletException {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
                "Authentication failed: " + failed.getLocalizedMessage());
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
            Authentication auth) throws IOException, ServletException {
        Map<String, Object> claims = new HashMap<>();
        claims.put("authorities",
                auth.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()));
        String token = jwtConfig.doGenerateToken(claims, auth.getName(), false);
        response.addHeader(jwtConfig.getHeader(), token);
        if (LOG.isDebugEnabled()) {
            String result = response.getHeader(jwtConfig.getHeader());
            LOG.debug("Authentication successful. Response with additional header: " + jwtConfig.getHeader()
                    + " and value: "
                    + (result != null && !result.isEmpty() ? "[PROTECTED]" : "N/A! This is impossible!"));
        }
    }
}