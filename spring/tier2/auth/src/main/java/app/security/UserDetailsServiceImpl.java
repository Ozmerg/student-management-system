package app.security;

import java.util.List;

import javax.naming.ServiceUnavailableException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import app.AuthApplication;
import general.dto.personEntities.Person;
import general.dto.pwdEntities.Pwd;
import general.errorHandling.GlobalExceptionHandler;
import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import io.github.resilience4j.timelimiter.annotation.TimeLimiter;

@Service
public class UserDetailsServiceImpl extends GlobalExceptionHandler implements UserDetailsService {

    @Autowired
    private RestTemplate restTemplate;

    private static final Logger LOG = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            Person person = getPersonByPersonName(username);
            Pwd pwd = getPwdByFkPerson(person.getId());
            List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                    .commaSeparatedStringToAuthorityList("ROLE_" + person.getRole());
            if (LOG.isDebugEnabled())
                LOG.debug("Person: " + person.getEmailAddress() + " with Pwd: "
                        + (pwd.getHashedPwd() != null && !pwd.getHashedPwd().isEmpty() ? "[PROTECTED]"
                                : "N/A! This is impossible!")
                        + " was found");
            return new User(username, pwd.getHashedPwd(), grantedAuthorities);
        } catch (HttpClientErrorException e) {
            LOG.warn("Couldn't find Person or Pwd entity for username " + username, e);
            throw new UsernameNotFoundException(e.getLocalizedMessage());
        } catch (ServiceUnavailableException e) {
            LOG.error("Service is currently unavailable", e);
            throw new UsernameNotFoundException(e.getLocalizedMessage());
        }
    }

    @CircuitBreaker(name = AuthApplication.RES4J_INSTANCE, fallbackMethod = "personServiceDown")
    @RateLimiter(name = AuthApplication.RES4J_INSTANCE)
    @Bulkhead(name = AuthApplication.RES4J_INSTANCE)
    @Retry(name = AuthApplication.RES4J_INSTANCE)
    @TimeLimiter(name = AuthApplication.RES4J_INSTANCE)
    public Person getPersonByPersonName(String username) throws ServiceUnavailableException {
        try {
            return restTemplate.getForObject(AuthApplication.PERSON_SERVICE_URL + username, Person.class);
        } catch (IllegalArgumentException e) {
            throw new ServiceUnavailableException(
                    String.format("No servers available for service: %s", AuthApplication.PERSON_SERVICE_URL));
        }
    }

    @CircuitBreaker(name = AuthApplication.RES4J_INSTANCE, fallbackMethod = "pwdServiceDown")
    @RateLimiter(name = AuthApplication.RES4J_INSTANCE)
    @Bulkhead(name = AuthApplication.RES4J_INSTANCE)
    @Retry(name = AuthApplication.RES4J_INSTANCE)
    @TimeLimiter(name = AuthApplication.RES4J_INSTANCE)
    public Pwd getPwdByFkPerson(Long fkPerson) throws ServiceUnavailableException {
        try {
            return restTemplate.getForObject(AuthApplication.PWD_SERVICE_URL + fkPerson, Pwd.class);
        } catch (IllegalArgumentException e) {
            throw new ServiceUnavailableException(
                    String.format("No servers available for service: %s", AuthApplication.PWD_SERVICE_URL));
        }
    }

    @SuppressWarnings("unused")
    private Person personServiceDown(String username, Exception e) throws ServiceUnavailableException {
        throw new ServiceUnavailableException(String.format("%s. No servers available for service: %s", e.getMessage(),
                AuthApplication.PERSON_SERVICE_URL));
    }

    @SuppressWarnings("unused")
    private Pwd pwdServiceDown(Long fkPerson, Exception e) throws ServiceUnavailableException {
        throw new ServiceUnavailableException(String.format("%s. No servers available for service: %s", e.getMessage(),
                AuthApplication.PWD_SERVICE_URL));
    }
}
