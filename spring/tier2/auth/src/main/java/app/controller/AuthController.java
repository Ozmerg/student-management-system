package app.controller;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.naming.ServiceUnavailableException;
import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import app.AuthApplication;
import general.dto.operationRequests.RegisterRequest;
import general.dto.personEntities.Person;
import general.dto.pwdEntities.Pwd;
import general.errorHandling.GlobalExceptionHandler;
import general.kafka.KafkaProducerService;
import general.kafka.KafkaTopicConstants;
import general.security.JwtConfig;
import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import io.github.resilience4j.timelimiter.annotation.TimeLimiter;
import io.jsonwebtoken.impl.DefaultClaims;

@Component
@RestController("AuthController")
public class AuthController extends GlobalExceptionHandler {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private JwtConfig jwtConfig;

    @Autowired
    private KafkaProducerService kafkaService;

    @Autowired
    private ObjectMapper objectMapper;

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @PostMapping("/auth/register")
    public Map<String, Boolean> registerPersonWithPassword(@Validated @RequestBody RegisterRequest fullPersonDetails)
            throws ServiceUnavailableException {
        Person person = postPerson(fullPersonDetails.getPerson());
        try {
            Pwd pwd = fullPersonDetails.getPwd();
            pwd.setFkPerson(person.getId());
            postPwd(pwd);
            return Map.of("registration complete for user: " + fullPersonDetails.getPerson().getEmailAddress(), true);
        } catch (ServiceUnavailableException e) {
            sendAuthenticationFailedMessage(person);
            throw e;
        }
    }

    @CircuitBreaker(name = AuthApplication.RES4J_INSTANCE, fallbackMethod = "personServiceDown")
    @RateLimiter(name = AuthApplication.RES4J_INSTANCE)
    @Bulkhead(name = AuthApplication.RES4J_INSTANCE)
    @Retry(name = AuthApplication.RES4J_INSTANCE)
    @TimeLimiter(name = AuthApplication.RES4J_INSTANCE)
    private Person postPerson(Person person) throws ServiceUnavailableException {
        try {
            return restTemplate.postForObject(AuthApplication.PERSON_SERVICE_URL, person, Person.class);
        } catch (IllegalArgumentException e) {
            throw new ServiceUnavailableException(
                    String.format("No servers available for service: %s", AuthApplication.PERSON_SERVICE_URL));
        }
    }

    @CircuitBreaker(name = AuthApplication.RES4J_INSTANCE, fallbackMethod = "pwdServiceDown")
    @RateLimiter(name = AuthApplication.RES4J_INSTANCE)
    @Bulkhead(name = AuthApplication.RES4J_INSTANCE)
    @Retry(name = AuthApplication.RES4J_INSTANCE)
    @TimeLimiter(name = AuthApplication.RES4J_INSTANCE)
    private Pwd postPwd(Pwd pwd) throws ServiceUnavailableException {
        try {
            return restTemplate.postForObject(AuthApplication.PWD_SERVICE_URL, pwd, Pwd.class);
        } catch (IllegalArgumentException e) {
            throw new ServiceUnavailableException(
                    String.format("No servers available for service: %s", AuthApplication.PWD_SERVICE_URL));
        }
    }

    @SuppressWarnings("unused")
    private Person personServiceDown(Person person, Exception e) throws ServiceUnavailableException {
        throw new ServiceUnavailableException(String.format("%s. No servers available for service: %s", e.getMessage(),
                AuthApplication.PERSON_SERVICE_URL));
    }

    @SuppressWarnings("unused")
    private Pwd pwdServiceDown(Pwd pwd, Exception e) throws ServiceUnavailableException {
        throw new ServiceUnavailableException(String.format("%s. No servers available for service: %s", e.getMessage(),
                AuthApplication.PWD_SERVICE_URL));
    }

    private void sendAuthenticationFailedMessage(Person person) {
        try {
            kafkaService.sendMessage(KafkaTopicConstants.AUTHENTICATION_FAILED,
                    objectMapper.writeValueAsString(person));
        } catch (JsonProcessingException e) {
            logger.error(String.format("Couldn't parse %s into String with ObjectMapper. Some Details: %s",
                    person.toString(), e.getMessage()));
        }
    }

    @PostMapping("auth/kafka")
    public ResponseEntity<?> test() {
        kafkaService.sendMessage(KafkaTopicConstants.AUTHENTICATION_FAILED, "hello there");
        return ResponseEntity.ok().build();
    }

    @PostMapping("auth/refreshtoken")
    public ResponseEntity<?> refreshToken(HttpServletRequest request) throws HttpClientErrorException {
        DefaultClaims claims = extractClaimsFromRequest(request);
        Map<String, Object> claimsMap = claims.entrySet().stream()
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
        String token = generateRefreshTokenWithChecks(claimsMap);
        return ResponseEntity.ok().header(jwtConfig.getHeader(), token).build();
    }

    private DefaultClaims extractClaimsFromRequest(HttpServletRequest request) throws HttpClientErrorException {
        try {
            return objectMapper.readValue(request.getHeader("claims"), DefaultClaims.class);
        } catch (JsonProcessingException e) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST,
                    "The provided \"claims\" can't be parsed into DefaultClaims.class");
        } catch (NullPointerException e) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST,
                    "The request doesn't contain the required \"claims\" header");
        }
    }

    private String generateRefreshTokenWithChecks(Map<String, Object> claimsMap) throws HttpClientErrorException {
        return jwtConfig.doGenerateToken(claimsMap,
                Optional.ofNullable(claimsMap.get("sub"))
                        .orElseThrow(() -> new HttpClientErrorException(HttpStatus.BAD_REQUEST,
                                "Entry \"sub\" is missing in provided claims"))
                        .toString(),
                true);
    }
}