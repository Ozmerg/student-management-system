package app;

import java.util.Locale;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import general.kafka.KafkaProducerService;
import general.security.EurekaHostnameVerifier;
import general.security.JwtConfig;

@EnableEurekaClient
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class })
public class AuthApplication {

	public static final String RES4J_INSTANCE = "authService";

	public static final String PERSON_SERVICE_URL = "https://person-service/person/";

	public static final String PWD_SERVICE_URL = "https://pwd-service/pwd/";

	@Value("${server.ssl.trust-store-password}")
	private String trustStoreKey;

	public static void main(String[] args) {
		SpringApplication.run(AuthApplication.class, args);
	}

	@Bean
	public LocaleResolver localeResolver() {
		SessionLocaleResolver slr = new SessionLocaleResolver();
		slr.setDefaultLocale(Locale.US);
		return slr;
	}

	@Bean
	public JwtConfig jwtConfig() {
		return new JwtConfig();
	}

	@Bean
	public KafkaProducerService kafkaProducerService() {
		return new KafkaProducerService();
	}

	@Bean
	public SSLContext sslContext() throws Exception {
		return new SSLContextBuilder()
				.loadTrustMaterial(ResourceUtils.getFile("classpath:x509/authtruststore.p12"),
						trustStoreKey.toCharArray())
				.loadKeyMaterial(ResourceUtils.getFile("classpath:x509/auth.p12"), trustStoreKey.toCharArray(),
						trustStoreKey.toCharArray())
				.build();
	}

	@Bean
	public CloseableHttpClient getCloseableHttpClient() throws Exception {
		SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(sslContext(),
				getEurekaHostnameVerifier());
		return HttpClients.custom().setSSLSocketFactory(socketFactory).build();
	}

	@Bean
	public HostnameVerifier getEurekaHostnameVerifier() {
		return new EurekaHostnameVerifier();
	}

	@Bean
	@LoadBalanced
	public RestTemplate getRestTemplate() throws Exception {
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(
				getCloseableHttpClient());
		return new RestTemplate(factory);
	}
}
