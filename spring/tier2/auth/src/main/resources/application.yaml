# mTLS - BEGIN
server:
    port: 8581
    ssl:
        enabled: true
        enabled-protocols: 
        - TLSv1.3
        ciphers:
        - TLS_AES_256_GCM_SHA384
        key-store: classpath:x509/auth.p12
        key-store-password: auth-service.key
        key-alias: auth-service
        key-password: auth-service.key
        key-store-type: PKCS12
        trust-store: classpath:x509/authtruststore.p12
        trust-store-password: auth-service.key
        trust-store-type: PKCS12
        client-auth: need
eureka:
    client:
        healthcheck:
            enabled: true
        serviceUrl:
            defaultZone: https://${eureka.instance.hostname:localhost}:${eureka.instance.secure-port:8761}/eureka
        tls:
            enabled: true
            key-store: classpath:x509/auth.p12
            key-store-password: auth-service.key
            key-password: auth-service.key
            key-store-type: PKCS12
            trust-store: classpath:x509/authtruststore.p12
            trust-store-password: auth-service.key
            trust-store-type: PKCS12
    instance: 
        hostname: localhost
        nonSecurePortEnabled: false
        securePortEnabled: true
        securePort: 8761
        secureVirtualHostName: eureka-service
# mTLS - END
spring:
    application.name: auth-service
    cloud.loadbalancer.ribbon.enabled: false
    main.banner-mode: "off"
    profiles.active: default
    # Activates the redirect of NoHandlerFoundException to Controller Advice - BEGIN
    web.resources.add-mappings: false
    mvc.throw-exception-if-no-handler-found: true
    # END 
    kafka:
        client-id: AuthService
        bootstrap-servers:
        - localhost:9092
        - localhost:9093
        - localhost:9094
        producer:
            bootstrap-servers:
            - localhost:9092
            - localhost:9093
            - localhost:9094
            client-id: AuthProducer
            key-serializer: org.apache.kafka.common.serialization.StringSerializer
            value-serializer: org.apache.kafka.common.serialization.StringSerializer
    
logging:
  level:
    '[org.springframework.kafka]': INFO
    '[org.springframework.web]': INFO
    '[app.security]': INFO
    '[javax.net.ssl]': TRACE

management:
    health:
        circuitbreakers:
            enabled: true
        ratelimiters:
            enabled: true
    endpoints:
        web:
            exposure:
                include: '*'
    endpoint:
        health:
            show-details: always
    metrics:
        tags:
            application: ${spring.application.name}
        distribution.percentiles-histogram:
            '[http.server.requests]': true
            '[resilience4j.circuitbreaker.calls]': true
            
resilience4j.circuitbreaker:
    configs:
        default:
            registerHealthIndicator: true
            slidingWindowSize: 10
            permittedNumberOfCallsInHalfOpenState: 3
            automaticTransitionFromOpenToHalfOpenEnabled: true
            slidingWindowType: TIME_BASED
            minimumNumberOfCalls: 20
            waitDurationInOpenState: 5s
            failureRateThreshold: 50
            eventConsumerBufferSize: 10
            recordExceptions:
                - org.springframework.web.client.HttpServerErrorException
                - java.util.concurrent.TimeoutException
                - java.io.IOException
    instances:
        authService:
            baseConfig: default

resilience4j.retry:
    configs:
        default:
            maxAttempts: 3
            waitDuration: 10s
            enableExponentialBackoff: true
            exponentialBackoffMultiplier: 2
            retryExceptions:
                - org.springframework.web.client.HttpServerErrorException
                - java.util.concurrent.TimeoutException
                - java.io.IOException
    instances:
        authService:
            baseConfig: default
                
resilience4j.bulkhead:
    configs:
        default:
            maxWaitDuration: 10ms
            maxConcurrentCalls: 20
    instances:
        authService:
            baseConfig: default
        
resilience4j.ratelimiter:
    configs:
        default:
            limitForPeriod: 10
            limitRefreshPeriod: 1s
            registerHealthIndicator: true
            eventConsumerBufferSize: 100
    instances:
        authService:
            baseConfig: default
            
resilience4j.timelimiter:
    configs:
        default:
            timeoutDuration: 2s
            cancelRunningFuture: true
    instances:
        authService:
            baseConfig: default

---
spring:
  config:
    activate:
      on-profile:
      - test
security.jwt.expiration: 0